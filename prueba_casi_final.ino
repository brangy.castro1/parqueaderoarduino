#include <ArduinoJson.h>
#include <RestClient.h>
#include <Ethernet.h>
#include <SPI.h>
#include "DHT.h"


#define delayTime 300  // Time in seconds beetwen sendings
#define IP "192.168.0.102" // Server IP
#define PORT 80     // Server Port

int trig = 3;
int echo = 2;
long tiempo = 0;
long dist = 0;

RestClient client = RestClient(IP, PORT);

//Setup
void setup() {
  Serial.begin(115200);
  // Connect via DHCP
  Serial.println("conectarse a la red.");
  client.dhcp();
  pinMode(trig, OUTPUT);
  pinMode(echo, INPUT);
  // Can still fall back to manual config:
  byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
  //the IP address for the shield:
  byte ip[] = { 192, 168, 0, 11 };
  Ethernet.begin(mac, ip);

  Serial.println("Setup!");
}

String response;
int cont = 1;

void loop() {
  response = "";
//  StaticJsonBuffer<200> jsonBuffer;
//  char json[256];
//  JsonObject& root = jsonBuffer.createObject();
//  root["plaza"] = "P01";
//  root["fecha_ingreso"] = "12-12-12 16:00";
//  root["fecha_salida"] = "12-12-12 17:00";
//  root.printTo(json, sizeof(json));
//  Serial.println(json);
    char json[] ="{\"sensor\":\"gps\",\"time\":1351824120,\"data\":[48.756080,2.302038]}";
    if (cont == 1) {
      int statusCode = client.post("/casa_abierta/auditoria", json, &response);
      Serial.print("Estado del envio: ");
      Serial.println(statusCode);
      Serial.print("Respuesta del servidor ");
      Serial.println(response);
      cont = 2;
      delay(200);
  }


  //getDistance();

}

void getDistance() {
  response = "";
  digitalWrite(trig, LOW);
  delayMicroseconds(5);
  digitalWrite(trig, HIGH);
  delayMicroseconds(10);
  digitalWrite(trig, LOW);

  tiempo = pulseIn(echo, HIGH);
  dist = (tiempo / 2) / 29;
  Serial.print("Distancia = "); Serial.print(dist);
  Serial.println(" cm");
  if (dist <= 10) {
    int statusCode = client.put("/casa_abierta/puestos/1", "estado=1", &response);
    Serial.print("Status code from server: ");
    Serial.println(statusCode);
    delay(200);
  } else {
    int statusCode = client.put("/casa_abierta/puestos/1", "estado=0", &response);
    Serial.print("Status code from server: ");
    Serial.println(statusCode);
    delay(200);
  }
}
